import requests
import logging
from common.config import format_logger

logger = logging.getLogger('gsheet')
format_logger(logger)

def get_master_spreadsheet(gsheet_url):
    logger.info('Acquiring spreadsheet')
    ret = []
    response = requests.get(gsheet_url)
    text = response.text
    for line in text.split('\n')[1:]:
        cols = line.split('\t')
        symbol, name, industry, ico, mineable = cols[:5]
        open_source, ico_raise, max_supply, hash_algorithm, blockchain_type = cols[5:10]
        disclosure, execution, potential, github_url, description = cols[10:15]
        website, whitepaper, contract_addr, twitter, reddit, telegram, _, _, _ = cols[15:]
        website = website.replace('\n','').replace('\r','')
        whitepaper = whitepaper.replace('\n','').replace('\r','')
        twitter = twitter.replace('\n','').replace('\r','')
        reddit = reddit.replace('\n','').replace('\r','')
        telegram = telegram.replace('\n','').replace('\r','')
        symbol = symbol.strip()
        name = name.strip()
        industry = industry.strip()
        hash_algorithm = hash_algorithm.strip()
        blockchain_type = blockchain_type.strip()
        contract_addr = contract_addr.strip()
        twitter = twitter.strip()
        reddit = reddit.strip()
        telegram = telegram.strip()
        if not ico.isdigit(): ico = 0
        if not mineable.isdigit(): mineable = 0
        if not open_source.isdigit(): open_source = 0
        if not ico_raise.isdigit(): ico_raise = 0
        if not max_supply.isdigit(): max_supply = 0
        if not disclosure.isdigit(): disclosure = 0
        if not execution.isdigit(): execution = 0
        if not potential.isdigit(): potential = 0
        ret.append({
            'symbol': symbol,
            'name': name,
            'ico': int(ico),
            'mineable': int(mineable),
            'industry': industry,
            'open_source': int(open_source),
            'ico_raise': int(ico_raise),
            'max_supply': int(max_supply),
            'hash_algorithm': hash_algorithm,
            'blockchain_type': blockchain_type,
            'disclosure': int(disclosure),
            'execution': int(execution),
            'potential': int(potential),
            'github_url': github_url,
            'description': description,
            'website_url': website,
            'whitepaper_url': whitepaper,
            'contract_address': contract_addr,
            'twitter': twitter,
            'reddit': reddit,
            'telegram': telegram
        })
    logger.debug('Parsed {} lines'.format(len(ret)))
    return ret
