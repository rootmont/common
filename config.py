import datetime
import logging

import os
import sys
import traceback
from multiprocessing.pool import Pool
from time import sleep

import pytz
utc = pytz.utc
os.environ['TZ'] = 'UTC'

import requests

logger = logging.getLogger('common')




def maybe_error(fxn, args):
    result = None
    try:
        result = fxn(*args)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        msg = '%s: %s' % (e, repr(traceback.extract_tb(exc_traceback)))
        logger.error('Function %s failed to execute with args %s:\n %s' % (fxn, str(args)[:5000], msg))
    return result


def maybe(fxn, args, num_processes=1):
    return \
    maybe_error(
        fxn=maybe_parallel,
        args=[fxn, args, num_processes > 1, num_processes]
    )


default_num_retries = 5


def maybe_retry(fxn, args, num_retries=default_num_retries, stall=0):
    if type(args) != list: args = [args]
    for i in range(num_retries - 1):
        try:
            return fxn(*args)
        except Exception as e:
            msg = 'Could not execute fxn %s with args %s..., this is the %d try' % (fxn, str(args)[:5000], i)
            logger.error(e)
            logger.error(msg)
            sleep(stall)
    return fxn(*args)


default_num_processes = 10


def maybe_parallel(fxn, args, parallel, num_processes=default_num_processes):
    if parallel:
        with Pool(num_processes) as p:
            results = p.map(fxn, args)
    else:
        results = [fxn(x) for x in args]
    return results


def date_interval(start, end, delta):
    curr = start
    while curr != end:
        yield curr
        curr += datetime.timedelta(days=delta)

def date_interval_reverse(start, end, delta):
    curr = end
    while curr != start:
        yield curr
        curr -= datetime.timedelta(days=delta)

def format_logger(logger):
    logger.setLevel(logging.DEBUG)
    logger.root.handlers.clear()
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def maybe_request(args, num_retries=5, stall=0):
    def check_status(url, args):
        response = requests.get(url, **args)
        if response.status_code != 200:
            raise ConnectionError
        return response.text
    return maybe_error(maybe_retry, [check_status, args, num_retries, stall])


format_logger(logger)

logger.debug('PYTHONPATH={}'.format(os.environ.get('PYTHONPATH')))
logger.debug('realpath={}'.format(os.path.realpath('.')))
logger.debug('listdir={}'.format(os.listdir('..')))

earliest_crypto_date = datetime.date(2019, 1, 1)
age_clusters = ['Emerging', 'Established', 'Mature']
mcap_clusters = ['Micro Cap', 'Small Cap', 'Medium Cap', 'Large Cap']
industry_clusters = ['Platform', 'Gaming', 'Computing', 'Exchange', 'Payments', 'Blockchain Interoperability', 'Asset Backed', 'Enterprise', 'Media', 'Public Services', 'IOT', 'Social', 'Finance', 'Privacy', 'Logistics']
earliest_ethereum_date = datetime.date(2015, 8, 8)
earliest_eos_date = datetime.date(2018, 6, 8)
gsheet_url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTGh4W0Nfq3v-AxXVXDoCNjrIx9Y8B5DO_nCjJaLlSTYhdV97fZYHfNu2JAPfl0-R_yMH5A3DQk0fn0/pub?gid=0&single=true&output=tsv'

# firefox ?
firefox_headers = {
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "cache-control": "max-age=0",
    "scheme": "https",
    "cookie": "__cfduid=d420f92bda370ea83f73a630429ee9d4f1524988388; _xicah=c9961c3a-fca776fa",
    "accept-language": "en-US",
    "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
    "method": "GET",
    "accept-encoding": "",
    "upgrade-insecure-requests": "1",
}
sliding_window_sizes = [7, 30, 90, 180, 360, 720, 10000]
