from db.connect import connection_wrap, get_latest_date
from db.coin import get_coin_id_for_name
import datetime
import logging
import pandas as pd
from typing import Union, List, Callable


logger = logging.getLogger('db')

@connection_wrap
def insert_rank(token_name: str, date: datetime.date, root_rank: float, risk_rank: float, social_rank: float, dev_rank: float, usage_rank: float, cursor=None):
    coin_id = get_coin_id_for_name(token_name)
    if coin_id is None: return
    sql = (
        "REPLACE INTO ranks "
        "SET coin_id = %d, my_date = DATE('%s'), root_rank = %f, risk_rank = %f, social_rank = %f, dev_rank = %f, usage_rank = %f "
        % (coin_id, date, root_rank, risk_rank, social_rank, dev_rank, usage_rank)
    )
    logger.debug(sql)
    cursor.execute(sql)


def get_latest_ranks(coin_id: int):
    max_date = get_latest_date(coin_id=coin_id, table='ranks', metric='root_rank')
    return get_ranks_for_date(coin_id, max_date)


@connection_wrap
def get_ranks_for_date(coin_id: int, date: datetime.date, cursor=None):
    sql = (
        "SELECT c.token_name, c.coin_id, root_rank, social_rank, dev_rank, risk_rank, usage_rank "
        "FROM ranks r "
        "INNER JOIN coin c "
        "ON c.coin_id = r.coin_id "
        "WHERE r.my_date = DATE('%s') "
        "AND c.coin_id = %d "
        % (date, coin_id)
    )
    cursor.execute(sql)
    ret = cursor.fetchone()
    return ret
    # df = pd.DataFrame(ret, columns=['root_rank', 'social_rank', 'dev_rank', 'risk_rank', 'usage_rank'], index=[x['token_name'] for x in ret])
    # return df


@connection_wrap
def get_ranks_for_names(token_names:List[str], start_date: datetime.date, cursor=None):
    datestring = start_date.strftime('%Y-%m-%d')
    sql = (
        "SELECT c.token_name, my_date, root_rank, social_rank, dev_rank, risk_rank, usage_rank "
        "FROM ranks r "
        "INNER JOIN coin c "
        "ON c.coin_id = r.coin_id "
        "WHERE r.my_date >= DATE('%s') "
        "AND c.token_name IN (%s) "
        % (datestring, '"{}"'.format('","'.join(token_names)))
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    df = pd.DataFrame(ret)
    return df


@connection_wrap
def get_ranks_since_date(start_date: datetime.date, cursor=None):
    datestring = start_date.strftime('%Y-%m-%d')
    sql = (
        "SELECT c.token_name, my_date, root_rank, social_rank, dev_rank, risk_rank, usage_rank "
        "FROM ranks r "
        "INNER JOIN coin c "
        "ON c.coin_id = r.coin_id "
        "WHERE r.my_date >= DATE('%s') "
        % (datestring)
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    df = pd.DataFrame(ret)
    return df

