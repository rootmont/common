from db.connect import connection_wrap, parse_frequency, get_latest_date
import datetime
from db.coin import get_coin_id_for_symbol, get_coin_id_for_name
from common.config import format_logger
import logging
import pymysql
import pandas as pd

logger = logging.getLogger('db.price_aggregation')
format_logger(logger)


@connection_wrap
def insert_hourly(
        tstamp:datetime.datetime,
        volume:int,
        symbol:str,
        open:float,
        high:float,
        low:float,
        close:float,
        marketcap:float,
        cursor=None
    ):
    coin_id = get_coin_id_for_symbol(symbol)
    if coin_id is None:
        return

    sql = (
        "REPLACE INTO price_hourly "
        "SET `tstamp` = TIMESTAMP('{}'), "
        "`coin_id` = {}, "
        "`price_open` = {}, "
        "`price_high` = {}, "
        "`price_low` = {}, "
        "`price_close` = {}, "
        "`trading_volume` = {}, "
        "`marketcap` = {} "
    ).format(tstamp.strftime('%Y-%m-%d %H:%M:%S'), coin_id, open, high, low, close, volume, marketcap)
    logger.debug(sql)
    cursor.execute(sql)


@connection_wrap
def insert_daily(
        my_date:datetime.date,
        volume:int,
        symbol:str,
        open:float,
        high:float,
        low:float,
        close:float,
        marketcap:float,
        cursor=None
    ):
    coin_id = get_coin_id_for_symbol(symbol)
    if coin_id is None:
        return

    sql = (
        "REPLACE INTO price_daily "
        "SET `my_date` = DATE('{}'), "
        "`coin_id` = {}, "
        "`price_open` = {}, "
        "`price_high` = {}, "
        "`price_low` = {}, "
        "`price_close` = {}, "
        "`trading_volume` = {}, "
        "`marketcap` = {} "
    ).format(my_date.strftime('%Y-%m-%d'), coin_id, open, high, low, close, volume, marketcap)
    logger.debug(sql)
    cursor.execute(sql)


@connection_wrap
def get_prices_frequency(frequency, cursor=None):
    freq = parse_frequency(frequency)
    sql = (
              "SELECT `tstamp`, `token_name`, `price_close` "
              "FROM price_hourly t "
              "INNER JOIN coin c "
              "ON c.coin_id=t.coin_id "
              "WHERE %s "
          ) % freq
    logger.debug(sql)
    cursor.execute(sql)
    ret = [x for x in cursor.fetchall() if x['token_name'] is not None]

    df = pd.DataFrame(ret, columns=['my_date', 'token_name', 'price_close'])
    # specifying values here prevents pandas from making a hierarchical index
    piv = df.pivot(index='my_date', columns='token_name', values='price_close')
    piv.fillna(0, inplace=True)
    return piv


@connection_wrap
def get_daily_prices(end_date=datetime.date.today(), days_back=1, cursor=None):
    start_date = end_date - datetime.timedelta(days=days_back)
    sql = (
        "SELECT `my_date`, `token_name`, `price_close` "
        "FROM price_daily t "
        "INNER JOIN coin c "
        "ON c.coin_id=t.coin_id "
        "WHERE my_date >= DATE('{}') "
        "AND my_date <= DATE('{}') "
    ).format(start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d'))

    logger.debug(sql)
    cursor.execute(sql)
    ret = cursor.fetchall()
    df = pd.DataFrame(ret, columns=['my_date', 'token_name', 'price_close'])
    return pd.pivot_table(df, index='my_date', columns='token_name', values='price_close')


@connection_wrap
def get_all_volumes(symbols, cursor=None):
    if type(symbols) == str: symbols = [symbols]
    escaped = [pymysql.escape_string(x) for x in symbols]
    condition = ' OR '.join(["ticker_symbol = '%s'" % x for x in escaped])

    sql = (
            "SELECT trading_volume, my_date, c.token_name "
            "FROM price_daily t "
            "INNER JOIN coin c "
            "ON t.coin_id = c.coin_id "
            "WHERE %s "
            % (condition)
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.DataFrame(ret, columns=["trading_volume", "my_date", "token_name"])
    return pd.pivot_table(df, index='my_date', columns='token_name', values='trading_volume', fill_value=0)


@connection_wrap
def get_all_marketcaps(symbols, cursor=None):
    if type(symbols) == str:
        symbols = [symbols]
    escaped = [pymysql.escape_string(x) for x in symbols]
    condition = ' OR '.join(["ticker_symbol = '%s'" % x for x in escaped])

    sql = (
            "SELECT marketcap, my_date, c.token_name "
            "FROM price_daily t "
            "INNER JOIN coin c "
            "ON t.coin_id = c.coin_id "
            "WHERE %s "
            % (condition)
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.DataFrame(ret, columns=["marketcap", "my_date", "token_name"])
    return pd.pivot_table(df, index='my_date', columns='token_name', values='marketcap', fill_value=0)


@connection_wrap
def get_all_price_close(symbols, cursor=None):
    if type(symbols) == str:
        symbols = [symbols]
    escaped = [pymysql.escape_string(x) for x in symbols]
    condition = ' OR '.join(["ticker_symbol = '%s'" % x for x in escaped])

    sql = (
            "SELECT price_close, my_date, c.token_name "
            "FROM price_daily t "
            "INNER JOIN coin c "
            "ON t.coin_id = c.coin_id "
            "WHERE %s "
            % (condition)
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.DataFrame(ret, columns=["price_close", "my_date", "token_name"])
    return pd.pivot_table(df, index='my_date', columns='token_name', values='price_close', fill_value=0)


@connection_wrap
def get_all_price_open(symbols, cursor=None):
    if type(symbols) == str: symbols = [symbols]
    escaped = [pymysql.escape_string(x) for x in symbols]
    condition = ' OR '.join(["ticker_symbol = '%s'" % x for x in escaped])

    sql = (
            "SELECT price_open, my_date, c.token_name "
            "FROM price_daily t "
            "INNER JOIN coin c "
            "ON t.coin_id = c.coin_id "
            "WHERE %s "
            % (condition)
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.DataFrame(ret, columns=["price_open", "my_date", "token_name"])
    return pd.pivot_table(df, index='my_date', columns='token_name', values='price_open', fill_value=0)


@connection_wrap
def get_earliest_prices(cursor=None):

    sql = (
            "SELECT min(my_date) as min_date, token_name "
            "FROM price_daily t "
            "INNER JOIN coin c "
            "ON t.coin_id = c.coin_id "
            "WHERE price_close > 0 "
            "GROUP BY token_name "
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.Series(data=[x['min_date'] for x in ret], index=[x["token_name"] for x in ret])
    return df


@connection_wrap
def get_latest_marketcaps(cursor=None):

    sql = (
            "SELECT t.my_date, s.max_date, t.token_name, t.marketcap "
            "FROM price_daily t "
            "INNER JOIN ("
                "SELECT max(my_date) as max_date, token_name "
                "FROM price_daily "
                "WHERE marketcap > 0 "
                "GROUP BY token_name ) s "
            "ON t.token_name = s.token_name "
            "AND t.my_date = s.max_date "
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    df = pd.DataFrame(ret, columns=['my_date', 'token_name', 'marketcap'])
    return df


@connection_wrap
def get_price_chart_data(symbol, cursor=None):
    coin_id = get_coin_id_for_symbol(symbol)
    if coin_id is None:
        return None
    today = datetime.datetime.today().strftime('%Y/%m/%d')

    sql = (
            "SELECT `my_date`, `price_close` as price, `marketcap`, `trading_volume` "
            "FROM price_daily "
            "WHERE coin_id = %d "
            "AND my_date <=  DATE('%s') "
            "ORDER BY `my_date` ASC"
            % (coin_id, today)
    )

    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.DataFrame(ret, columns=["price", "my_date", "marketcap", "trading_volume"])
    df.index = df["my_date"]
    return df


@connection_wrap
def get_price_volume_for_name(token_name, cursor=None):
    coin_id = get_coin_id_for_name(token_name)
    if coin_id is None:
        return None
    today = datetime.datetime.today().strftime('%Y/%m/%d')

    sql = (
            "SELECT `my_date`, `price_close` as price, `trading_volume` as volume "
            "FROM price_daily "
            "WHERE coin_id = %d "
            "AND my_date <=  DATE('%s') "
            "ORDER BY `my_date` ASC"
            % (coin_id, today)
    )

    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.DataFrame(ret, columns=["price_close", "my_date", "trading_volume"])
    df.index = df["my_date"]
    df.drop(columns=['my_date'], inplace=True)

    return df


@connection_wrap
def get_latest_price_metrics(days_back=7, cursor=None):
    end_date = get_latest_date(metric='price_close', table='price_daily')
    start_date = end_date - datetime.timedelta(days=days_back)
    sql = (
        "SELECT price_close, trading_volume, marketcap, my_date, token_name "
        "FROM price_daily t "
        "INNER JOIN coin c "
        "ON t.coin_id = c.coin_id "
        "WHERE my_date <= DATE('%s') "
        "AND my_date >= DATE('%s') "
        % (end_date.strftime('%Y-%m-%d'), start_date.strftime('%Y-%m-%d'))
    )

    cursor.execute(sql)
    ret = list(cursor.fetchall())
    df = pd.DataFrame(ret, columns=["price_close", "trading_volume", "marketcap", "my_date", "token_name"])
    return df


@connection_wrap
def get_global_marketcaps(start_date: datetime.date, cursor=None):
    datestring = start_date.strftime('%Y-%m-%d')
    sql = (
        "SELECT sum(marketcap) as global_marketcap, my_date "
        "FROM price_daily "
        "WHERE my_date >= DATE('%s') "
        "GROUP BY my_date "
        % datestring
    )
    logger.debug(sql)
    cursor.execute(sql)
    ret = cursor.fetchall()
    df = pd.DataFrame(ret)
    return df


@connection_wrap
def get_weekly_update_movers(start_date: datetime.date, cursor=None):
    datestring = start_date.strftime('%Y-%m-%d')
    sql = (
        "SELECT marketcap, trading_volume, price_open, token_name, my_date "
        "FROM price_daily "
        "WHERE my_date >= DATE('%s') "
        % datestring
    )
    logger.debug(sql)
    cursor.execute(sql)
    ret = cursor.fetchall()
    df = pd.DataFrame(ret)
    return df

