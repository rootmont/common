import json
import logging
from rank_tree import rank_tree

import pandas as pd
from db.connect import connection_wrap

import datetime
from typing import Union, List

logger = logging.getLogger('db')

all_data = None
last_updated = None

def parse_github_date(date_str: str) -> datetime.datetime:
    return datetime.datetime.strptime(date_str[:len(date_str) - 1], "%Y-%m-%dT%H:%M:%S")


class GithubIssue(object):
    """
    Helper class for accumulating information about historical issue information
    """
    def __init__(self, created_at: Union[datetime.datetime, str], closed_at: Union[datetime.datetime, str, None]):
        if type(created_at) == str:
            self._created_at: datetime.datetime = parse_github_date(created_at)
        else:
            self._created_at: datetime.datetime = created_at

        if type(closed_at) == str:
            self._closed_at: datetime.datetime = parse_github_date(closed_at)
        elif closed_at is None:
            self._closed_at = None
        else:
            self._closed_at: datetime.datetime = closed_at

    def __hash__(self):
        return hash(self.created_at.__hash__() + self.closed_at.__hash__()) if self.closed_at is not None else hash(self.created_at.__hash__())

    def __eq__(self, other):
        if type(other) != GithubIssue:
            return False
        if other.created_at == self._created_at and self._closed_at == other.closed_at:
            return True
        return False

    @property
    def created_at(self):
        return self._created_at

    @property
    def closed_at(self):
        return self._closed_at

    def closed_before(self, d: Union[datetime.datetime, datetime.date]) -> bool:
        if self._closed_at is None:
            return False
        if type(d) == datetime.datetime:
            d = d.date()
        return self._closed_at.date() <= d

    def created_before(self, d: Union[datetime.datetime, datetime.date]) -> bool:
        if type(d) == datetime.datetime:
            d = d.date()
        return self._created_at.date() <= d

    def created_after(self, d: Union[datetime.datetime, datetime.date]) -> bool:
        if type(d) == datetime.datetime:
            d = d.date()
        return self._created_at.date() > d


class GithubCommit(object):
    def __init__(self, name: str, date: Union[datetime.datetime, str]):
        self._name = name
        if type(date) is str:
            self._date: datetime.datetime = parse_github_date(date)
        else:
            self._date: datetime.datetime = date

    def __eq__(self, other):
        if type(other) != GithubCommit:
            return False
        if other.name == self.name and self._date == other.date:
            return True
        return False

    def __hash__(self):
        return hash(self._name.__hash__() + self._date.__hash__())

    def __repr__(self):
        return "GithubCommit<name: {}, date: {}>".format(self._name, self._date)

    @property
    def name(self):
        return self._name

    @property
    def date(self):
        return self._date

    def created_before(self, day: Union[datetime.date, datetime.datetime]) -> bool:
        if type(day) == datetime.datetime:
            day = day.date()
        return self._date.date() <= day


class GithubComment(object):
    def __init__(self, name: str, date: Union[datetime.datetime, str]):
        self._name = name
        if type(date) is str:
            self._date: datetime.datetime = parse_github_date(date)
        else:
            self._date: datetime.datetime = date

    def __eq__(self, other):
        if type(other) != GithubComment:
            return False
        if other.name == self._name and self._date == other.date:
            return True
        return False

    def __hash__(self):
        return hash(self._name.__hash__() + self._date.__hash__())

    @property
    def name(self):
        return self._name

    @property
    def date(self):
        return self._date

    def created_after(self, day: Union[datetime.date, datetime.datetime]) -> bool:
        if type(day) == datetime.datetime:
            day = day.date()
        return self._date.date() > day

    def created_before(self, day: Union[datetime.date, datetime.datetime]) -> bool:
        if type(day) == datetime.datetime:
            day = day.date()
        return self._date.date() <= day


class GithubData(object):
    def __init__(self,
                 coin_id: int,
                 project_repo_url: str,
                 forks: List[datetime.datetime],
                 stars: List[datetime.datetime],
                 issues: List[GithubIssue],
                 commits: List[GithubCommit],
                 comments: List[GithubComment]):
        self._coin_id: int = coin_id
        self._project_repo_url: str = project_repo_url
        self._forks: List[datetime.datetime] = list(set(forks))
        self._stars: List[datetime.datetime] = list(set(stars))
        self._issues: List[GithubIssue] = list(set(issues))
        self._commits: List[GithubCommit] = list(set(commits))
        self._comments: List[GithubComment] = list(set(comments))

        self.__sort_all()

    def __add__(self, other):
        self._forks.extend(other.forks)
        self._stars.extend(other.stars)
        self._issues.extend(other.issues)
        self._commits.extend(other.commits)
        self._comments.extend(other.comments)

        # Make sure we have unique
        self._forks = list(set(self._forks))
        self._stars = list(set(self._stars))
        self._issues = list(set(self._issues))
        self._commits = list(set(self._commits))
        self._comments = list(set(self._comments))

        # sort everything so we can do lookups
        self.__sort_all()

        return self

    def __eq__(self, other):
        for f in self._forks:
            if f not in other.forks:
                return False

        for s in self._stars:
            if s not in other.stars:
                return False

        for i in self._issues:
            if i not in other.issues:
                return False

        for c in self._commits:
            if c not in other.commits:
                return False

        for comment in self._comments:
            if comment not in other.comments:
                return False

        if self._project_repo_url != other.project_repo_url:
            return False

        if self._coin_id != other.coin_id:
            return False

        return True

    def __sort_all(self):
        # sort all the lists
        self._forks.sort(reverse=True)
        self._stars.sort(reverse=True)
        self._issues.sort(key=lambda x: x.created_at, reverse=True)
        self._commits.sort(key=lambda x: x.date, reverse=True)
        self._comments.sort(key=lambda x: x.date, reverse=True)

    @property
    def coin_id(self):
        return self._coin_id

    @property
    def forks(self):
        return self._forks

    @property
    def stars(self):
        return self._stars

    @property
    def issues(self):
        return self._issues

    @property
    def commits(self):
        return self._commits

    @property
    def comments(self):
        return self._comments

    @property
    def project_repo_url(self):
        return self._project_repo_url

    @project_repo_url.setter
    def project_repo_url(self, x: str):
        self._project_repo_url = x

    @staticmethod
    def _parse_datetime_date(day: Union[datetime.date, datetime.datetime]) -> datetime.date:
        if type(day) == datetime.datetime:
            return day.date()
        return day

    def all_forks(self) -> int:
        return len(self._forks)

    def all_stars(self) -> int:
        return len(self._stars)

    def all_issues(self) -> int:
        return len(self._issues)

    def all_commits(self) -> int:
        return len(self._commits)

    def all_comments(self) -> int:
        return len(self._comments)

    def last_commit(self) -> Union[None, GithubCommit]:
        return None if len(self._commits) == 0 else self._commits[0]

    def all_contributions(self) -> int:
        return len(self._comments) + len(self._commits)

    def all_unique_contributors(self) -> int:
        return len(set(map(lambda x: x.name, self._commits)))

    def forks_before(self, day: Union[datetime.date, datetime.datetime]) -> int:
        day = self._parse_datetime_date(day)
        return len(list(filter(lambda x: x.date() <= day, self._forks)))

    def stars_before(self, day: Union[datetime.date, datetime.datetime]) -> int:
        day = self._parse_datetime_date(day)
        return len(list(filter(lambda x: x.date() <= day, self._stars)))

    def open_issues_before(self, day: Union[datetime.date, datetime.datetime]) -> int:
        day = self._parse_datetime_date(day)
        return len(list(filter(lambda x: x.created_before(day) and not x.closed_before(day), self._issues)))

    def commits_before(self, day: Union[datetime.date, datetime.datetime]) -> int:
        day = self._parse_datetime_date(day)
        return len(list(filter(lambda x: x.created_before(day), self._commits)))

    def comments_before(self, day: Union[datetime.date, datetime.datetime]) -> int:
        day = self._parse_datetime_date(day)
        return len(list(filter(lambda x: x.created_before(day), self._comments)))

    def unique_contributors_before(self, day: Union[datetime.date, datetime.datetime]) -> int:
        day = self._parse_datetime_date(day)
        return len(set(map(lambda x: x.name, filter(lambda x: x.created_before(day), self._commits))))

    def last_commit_before(self, day: Union[datetime.date, datetime.datetime]) -> Union[None, GithubCommit]:
        day = self._parse_datetime_date(day)
        for commit in self._commits:
            if commit.date.date() <= day:
                return commit
        return None

    #TODO: put this in db and coin report
    def days_since_last_commit(self) -> int:
        last_commit_day = max([x.date.date() for x in self._commits])
        today = datetime.date.today()
        return (last_commit_day - today).days

    def contributions_before(self, day: Union[datetime.date, datetime.datetime]) -> int:
        return self.comments_before(day) + self.commits_before(day)


@connection_wrap
def insert_for_github_data(github_data, cursor=None):
    sql = (
            "INSERT INTO `github_stats` "
            "SET `coin_id` = %s,"
            "`repo_url` = %s, "
            "`forks` = %s, "
            "`stars` = %s, "
            "`issues` = %s, "
            "`commits` = %s, "
            "`comments` = %s "
            "ON DUPLICATE KEY UPDATE "
            "`repo_url` = %s, "
            "`forks` = %s, "
            "`stars` = %s, "
            "`issues` = %s, "
            "`commits` = %s, "
            "`comments` = %s"
    )

    forks = [f.timestamp() for f in github_data.forks]
    stars = [s.timestamp() for s in github_data.stars]
    issues = [{
        'created_at': i.created_at.timestamp(),
        'closed_at': i.closed_at.timestamp() if i.closed_at else None
    } for i in github_data.issues]
    commits = [{'name': c.name, 'date': c.date.timestamp()} for c in github_data.commits]
    comments = [{'name': c.name, 'date': c.date.timestamp()} for c in github_data.comments]

    cursor.execute(sql, [
        github_data.coin_id, github_data.project_repo_url, json.dumps(forks), json.dumps(stars), json.dumps(issues), json.dumps(commits), json.dumps(comments),
        github_data.project_repo_url, json.dumps(forks), json.dumps(stars), json.dumps(issues), json.dumps(commits), json.dumps(comments)
    ])


def parse_github_data(row) -> GithubData:
    forks = [datetime.datetime.utcfromtimestamp(f) for f in json.loads(row["forks"])]
    stars = [datetime.datetime.utcfromtimestamp(s) for s in json.loads(row["stars"])]
    issues = [GithubIssue(
        datetime.datetime.utcfromtimestamp(i["created_at"]),
        datetime.datetime.utcfromtimestamp(i["closed_at"]) if i["closed_at"] is not None else None) for i in
        json.loads(row["issues"])]
    commits = [GithubCommit(i["name"], datetime.datetime.utcfromtimestamp(i["date"])) for i in json.loads(row["commits"])]
    comments = [GithubComment(i["name"], datetime.datetime.utcfromtimestamp(i["date"])) for i in
                json.loads(row["comments"])]
    return GithubData(row["coin_id"], row["repo_url"], forks, stars, issues, commits, comments)


@connection_wrap
def to_data_tables(github_data: List[GithubData], cutoff_date=datetime.date.today(), cursor=None):
    sql = "SELECT coin_id, token_name from coin"
    cursor.execute(sql)
    ret = cursor.fetchall()

    ids_to_token_names = {x['coin_id']: x['token_name'] for x in ret}

    df_data = []
    for gd in github_data:
        t = dict()
        t['github_stargazers'] = gd.stars_before(cutoff_date)
        t['github_forks'] = gd.forks_before(cutoff_date)
        t['github_open_issues'] = gd.open_issues_before(cutoff_date)
        t['github_contributors'] = gd.unique_contributors_before(cutoff_date)
        t['github_contributions'] = gd.contributions_before(cutoff_date)
        t['token_name'] = ids_to_token_names[gd.coin_id]
        df_data.append(t)

    df = pd.DataFrame(data=df_data, index=[x['token_name'] for x in df_data],
                 columns=['github_stargazers', 'github_forks', 'github_open_issues', 'github_contributors', 'github_contributions']).fillna(0)
    desired_cols = set(rank_tree['root_rank']['dev_rank'].values())
    actual_cols = set(df.columns)
    drop_cols = actual_cols - desired_cols
    if len(desired_cols - actual_cols) > 0:
        logger.warning('Missing desired columns in github db {}'.format(desired_cols - actual_cols))
    df.drop(columns=drop_cols, inplace=True)
    metric_rank = df.rank(axis='index', pct=True)
    dev_rank = metric_rank.mean(axis='columns')
    return df, metric_rank, dev_rank


@connection_wrap
def get_for_coin_id(coin_id, cursor=None) -> Union[None, GithubData]:
    sql = "SELECT * FROM github_stats WHERE coin_id = %s"
    cursor.execute(sql, [coin_id])
    ret = cursor.fetchone()

    if ret is None:
        return None

    return parse_github_data(ret)


@connection_wrap
def get_for_all_coins(cursor=None) -> Union[None, List[GithubData]]:
    # global all_data
    # global last_updated
    # now = datetime.datetime.now()
    # if all_data is not None and (now - last_updated).days < 1:
    #     return all_data
    sql = "SELECT * FROM github_stats"
    cursor.execute(sql)
    ret = cursor.fetchall()

    if ret is None:
        return None

    all_data = [parse_github_data(r) for r in ret]
    return all_data