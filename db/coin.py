from db.connect import connection_wrap
import logging
import pymysql
import pandas as pd
logger = logging.getLogger('db')


@connection_wrap
def coin_exists(name, cursor=None):
    sql = "SELECT `coin_id` FROM coin WHERE `token_name` LIKE '%s'" % (name)
    cursor.execute(sql)
    ret = cursor.fetchone()
    return ret is not None


@connection_wrap
def get_coin_id_for_symbol(symbol, cursor=None):
    symbol = pymysql.escape_string(symbol)
    sql = "SELECT `coin_id` from coin where `ticker_symbol` like '%s' LIMIT 1" % symbol
    cursor.execute(sql)
    ret = cursor.fetchone()
    if ret is None:
        logger.warning('No coin id found for symbol', symbol)
        return None
    coin_id = ret["coin_id"]
    return coin_id


@connection_wrap
def get_coin_ids_for_symbols(symbols, cursor=None):
    escaped = [pymysql.escape_string(x) for x in symbols]
    conditions = ["`ticker_symbol` LIKE '%s'" % x for x in escaped]
    condition = ' OR '.join(conditions)
    sql = "SELECT `coin_id` FROM coin WHERE " + condition
    cursor.execute(sql)
    ret = cursor.fetchall()
    if ret is None:
        logger.warning('No coin ids found for symbols', symbols)
        return None
    coin_ids = [x["coin_id"] for x in ret]
    return coin_ids


@connection_wrap
def get_all_coin_ids_for_symbol(symbol, cursor=None):
    symbol = pymysql.escape_string(symbol)
    sql = "SELECT `coin_id` from coin where `ticker_symbol` like '%s'" % symbol
    cursor.execute(sql)
    ret = cursor.fetchall()
    if ret is None:
        logger.warning('No coin id found for ', symbol)
        return None
    return ret


@connection_wrap
def get_coin_id_for_name(name, cursor=None):
    name = pymysql.escape_string(name)
    sql = "SELECT `coin_id` from `coin` where `token_name` like '%s'" % name
    cursor.execute(sql)
    ret = cursor.fetchone()
    if ret is None:
        logger.warning('No coin id found for name', name)
        return None
    coin_id = ret["coin_id"]
    return coin_id


@connection_wrap
def get_coin_id_for_twitters(cursor=None):
    sql = "SELECT `coin_id`, `twitter` from `coin`"
    cursor.execute(sql)
    ret = cursor.fetchall()
    return {x['twitter']: x['coin_id'] for x in ret}


@connection_wrap
def get_name_for_twitters(cursor=None):
    sql = "SELECT `token_name`, `twitter` from `coin`"
    cursor.execute(sql)
    ret = cursor.fetchall()
    return {x['twitter']: x['token_name'] for x in ret}


@connection_wrap
def insert_coin_industry(name, industry, cursor=None):
    coin_id = get_coin_id_for_name(name)
    if coin_id is None: return None
    industry = pymysql.escape_string(industry)
    sql = (
        "INSERT INTO `coin_category` (`coin_id`,`category`) " 
        "VALUES (%d,'%s') "
        "ON DUPLICATE KEY UPDATE "
        "`category` = '%s'"
        % (coin_id, industry, industry)
    )
    cursor.execute(sql)


@connection_wrap
def get_category_for_coin(name, cursor=None):
    name = pymysql.escape_string(name)
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return None
    sql = "SELECT `category` from coin_category where `coin_id` = %d" % coin_id
    cursor.execute(sql)
    ret = cursor.fetchone()
    category = '' if ret is None else ret["category"]
    return category


@connection_wrap
def insert_coin(
    company_name,
    token_name,
    symbol,
    ico,
    mineable,
    open_source,
    ico_raise,
    max_supply,
    hash_algorithm,
    blockchain_type,
    disclosure,
    execution,
    potential,
    description,
    website_url,
    whitepaper_url,
    twitter,
    reddit,
    telegram,
    cursor=None
):
    
    company_name = pymysql.escape_string(company_name)
    token_name = pymysql.escape_string(token_name)
    symbol = pymysql.escape_string(symbol)
    hash_algorithm = pymysql.escape_string(hash_algorithm)
    blockchain_type = pymysql.escape_string(blockchain_type)
    description = pymysql.escape_string(description)
    description = "".join(i for i in description if ord(i) < 128)
    args = (company_name,token_name,symbol,ico,mineable,open_source,ico_raise,max_supply,hash_algorithm,blockchain_type,disclosure,execution,potential,description,website_url,whitepaper_url,twitter,reddit,telegram)
    sql = (
        "INSERT INTO `coin` "
        "SET `company_name` = '%s', `token_name` = '%s', `ticker_symbol` = '%s', "
        "`ico` = %d, `mineable` = %d,`open_source` = %d,`ico_raise` = %d, `max_supply` = %d, "
        "`hash_algorithm` = '%s',`blockchain_type` = '%s', "
        "`disclosure` = %d,`execution` = %d,`potential` = %d, "
        "`description` = '%s', `website_url` = '%s', `whitepaper_url` = '%s', "
        "`twitter` = '%s', `reddit` = '%s', `telegram` = '%s'"
        % args
    )
    cursor.execute(sql)


@connection_wrap
def update_coin(
    company_name,
    token_name,
    symbol,
    ico,
    mineable,
    open_source,
    ico_raise,
    max_supply,
    hash_algorithm,
    blockchain_type,
    disclosure,
    execution,
    potential,
    description,
    website_url,
    whitepaper_url,
    twitter,
    reddit,
    telegram,
    cursor=None
):
    
    coin_id = get_coin_id_for_name(token_name)
    if coin_id is None: return None
    company_name = pymysql.escape_string(company_name)
    token_name = pymysql.escape_string(token_name)
    symbol = pymysql.escape_string(symbol)
    hash_algorithm = pymysql.escape_string(hash_algorithm)
    blockchain_type = pymysql.escape_string(blockchain_type)
    description = pymysql.escape_string(description)
    description = "".join(i for i in description if ord(i) < 128)
    args = (company_name,token_name,symbol,ico,mineable,open_source,ico_raise,max_supply,hash_algorithm,blockchain_type,disclosure,execution,potential,description,website_url,whitepaper_url,twitter,reddit,telegram,coin_id)
    sql = (
        "UPDATE `coin` "
        "SET `company_name` = '%s', `token_name` = '%s', `ticker_symbol` = '%s',"
        "`ico` = %d, `mineable` = %d,`open_source` = %d,`ico_raise` = %d, `max_supply` = %d,"
        "`hash_algorithm` = '%s',`blockchain_type` = '%s',"
        "`disclosure` = %d,`execution` = %d,`potential` = %d,"
        "`description` = '%s', `website_url` = '%s', `whitepaper_url` = '%s', "
        "`twitter` = '%s', `reddit` = '%s', `telegram` = '%s'"
        "WHERE `coin_id` = %d"
        % args
    )
    cursor.execute(sql)


@connection_wrap
def get_coin_info_all(cursor=None):
    sql = (
        "SELECT * FROM coin c " 
        "INNER JOIN coin_category cc "
        "ON c.coin_id = cc.coin_id"
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


@connection_wrap
def get_name_for_coin_id(coin_id, cursor=None):
    sql = "SELECT token_name from coin where coin_id = %d" % coin_id
    cursor.execute(sql)
    name = cursor.fetchone()
    if name is None:
        ret = None
    else:
        ret = name['token_name']
    return ret


@connection_wrap
def get_coin_info_symbol(symbol, cursor=None):
    coin_id = get_coin_id_for_symbol(symbol)
    if coin_id is None:
        return None
    sql = "SELECT * from coin where coin_id = %d" % coin_id
    cursor.execute(sql)
    ret = cursor.fetchone()
    
    return ret


@connection_wrap
def get_coin_info_industry(industry, cursor=None):
    industry = pymysql.escape_string(industry)
    sql = (
        "SELECT * from coin "
        "RIGHT JOIN coin_category "
        "ON coin_category.coin_id = coin.coin_id "
        "WHERE category = '%s'"
        % industry
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


@connection_wrap
def get_coin_info_marketcap(mcap, cursor=None):
    mcap = pymysql.escape_string(mcap)
    sql = (
        "SELECT * from coin "
        "RIGHT JOIN coin_category "
        "ON coin_category.coin_id = coin.coin_id "
        "WHERE marketcap_cluster = '%s'"
        % mcap
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    
    return ret


@connection_wrap
def get_coin_info_age(age, cursor=None):
    age = pymysql.escape_string(age)
    sql = (
        "SELECT * from coin "
        "RIGHT JOIN coin_category "
        "ON coin_category.coin_id = coin.coin_id "
        "WHERE age_cluster = '%s'"
        % age
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    
    return ret


@connection_wrap
def get_name_for_symbol(symbol, cursor=None):
    # symbol = pymysql.escape_string(symbol)
    sql = (
        "SELECT token_name from coin "
        "WHERE ticker_symbol = %s"
    )
    cursor.execute(sql, symbol)
    ret = cursor.fetchall()

    if len(ret) == 0:
        return None
    elif len(ret) > 1:
        logger.warning('more than one name returned for symbol %s, %s' % (symbol, ret))
    return ret[0]['token_name']


@connection_wrap
def get_all_marketcap_clusters(cursor=None):
    sql = "SELECT DISTINCT(`marketcap_cluster`) FROM coin WHERE `marketcap_cluster` IS NOT NULL"
    cursor.execute(sql)
    ret = cursor.fetchall()
    
    return ret


@connection_wrap
def get_all_age_clusters(cursor=None):
    sql = "SELECT DISTINCT(`age_cluster`) FROM coin WHERE `age_cluster` IS NOT NULL"
    cursor.execute(sql)
    ret = cursor.fetchall()
    
    return ret


@connection_wrap
def get_all_symbols(cursor=None):
    sql = "SELECT DISTINCT(`ticker_symbol`), `token_name` from coin"
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


@connection_wrap
def get_all_coins(cursor=None):
    sql = "SELECT ticker_symbol from coin"
    cursor.execute(sql)
    ret = cursor.fetchall()
    if ret is None:
        return []
    return ret


@connection_wrap
def get_all_coin_names(cursor=None):
    sql = "SELECT token_name from coin"
    cursor.execute(sql)
    ret = cursor.fetchall()
    if ret is None:
        return []
    return [x['token_name'] for x in ret]


@connection_wrap
def get_all_coin_info(cursor=None):
    sql = "SELECT * from coin"
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


@connection_wrap
def get_all_categories(cursor=None):
    sql = "SELECT DISTINCT(`category`) from coin_category"
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


@connection_wrap
def insert_description(name, description, cursor=None):
    
    coin_id = get_coin_id_for_name(name)
    if coin_id is None: return
    description = pymysql.escape_string(description)
    description = "".join(i for i in description if ord(i) < 128)
    
    sql = (
        "UPDATE `coin` "
        "SET `description` = '%s' "
        "WHERE `coin_id` = %d"
        % (description, coin_id)
    )
    cursor.execute(sql)


@connection_wrap
def get_description(name, cursor=None):
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return
    
    sql = (
        "SELECT `description` "
        "FROM `coin` "
        "WHERE `coin_id` = %d"
        % (coin_id)
    )
    cursor.execute(sql)
    ret = cursor.fetchone()
    return ret


def delete_coin_symbol(symbol):
    coin_ids = get_all_coin_ids_for_symbol(symbol)
    if len(coin_ids) == 0:
        return None
    for coin_id in coin_ids:
        delete_coin_id(coin_id)


def delete_coin_name(name):
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return None
    delete_coin_id(coin_id)


@connection_wrap
def delete_coin_id(coin_id, cursor=None):
    sql = "DELETE FROM `coin_category` WHERE `coin_id` = %d" % coin_id
    cursor.execute(sql)

    sql = "DELETE FROM `coin` WHERE `coin_id` = %d" % coin_id
    cursor.execute(sql)

    sql = "DELETE FROM `risk` WHERE `coin_id` = %d" % coin_id
    cursor.execute(sql)

    sql = "DELETE FROM `daily_stats` WHERE `coin_id` = %d" % coin_id
    cursor.execute(sql)

    sql = "DELETE FROM `timeseries` WHERE `coin_id` = %d" % coin_id
    cursor.execute(sql)


@connection_wrap
def delete_category(category, cursor=None):
    
    category = pymysql.escape_string(category)
    
    sql = "DELETE FROM `coin_category` WHERE `category` = '%s'" % category
    cursor.execute(sql)


@connection_wrap
def insert_execution_potential(symbol, execution, potential, cursor=None):
    coin_id = get_coin_id_for_symbol(symbol)
    if coin_id is None:
        return None
    
    sql = "UPDATE `coin` SET `execution` = %f, `potential` = %f WHERE `coin_id` = %d" % (
        execution,
        potential,
        coin_id,
    )
    cursor.execute(sql)


@connection_wrap
def insert_cluster_columns(name, mcap_cluster, age_cluster, cursor=None):
    
    
    mcap_cluster = pymysql.escape_string(mcap_cluster)
    age_cluster = pymysql.escape_string(age_cluster)
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return None
    sql = (
        "UPDATE `coin` SET `marketcap_cluster` = '%s', `age_cluster` = '%s' "
        "WHERE `coin_id` = %d"
        % (mcap_cluster, age_cluster, coin_id)
    )
    cursor.execute(sql)


@connection_wrap
def get_disclosure(name, cursor=None):
    name = pymysql.escape_string(name)
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return None
    sql = (
        "SELECT `disclosure` "
        "FROM coin "
        "WHERE `coin_id` = %d"
        % (coin_id)
    )
    cursor.execute(sql)
    ret = cursor.fetchone()
    return ret['disclosure']


@connection_wrap
def get_disclosure_all(cursor=None):
    sql = (
        "SELECT `token_name` "
        "FROM coin "
        "WHERE `disclosure` = 1"
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


@connection_wrap
def insert_disclosure(name, disclosure, cursor=None):
    
    
    name = pymysql.escape_string(name)
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return None
    sql = (
        "UPDATE coin "
        "SET `disclosure` = %d "
        "WHERE `coin_id` = %d"
        % (disclosure, coin_id)
    )
    cursor.execute(sql)


@connection_wrap
def get_all_clusters(cursor=None):
    sql = (
            "SELECT token_name, marketcap_cluster, age_cluster, category as industry_cluster "
            "FROM coin "
            "INNER JOIN coin_category "
            "ON coin.coin_id = coin_category.coin_id "
    )
    cursor.execute(sql)
    ret = cursor.fetchall()

    return pd.DataFrame(ret, columns=['marketcap_cluster', 'age_cluster', 'industry_cluster'], index=[x['token_name'] for x in ret])


@connection_wrap
def get_clusters(name, cursor=None):
    name = pymysql.escape_string(name)
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return None
    sql = (
        "SELECT `marketcap_cluster`, `age_cluster` "
        "FROM coin "
        "WHERE `coin_id` = %d"
        % (coin_id)
    )
    cursor.execute(sql)
    ret = cursor.fetchone()
    return ret


@connection_wrap
def get_timeseries_records_count(coin_id: int, cursor=None):
    sql = "SELECT count(*) FROM `timeseries` WHERE coin_id = %s"
    cursor.execute(sql, [coin_id])
    return int(cursor.fetchone()['count(*)'])


@connection_wrap
def get_my_date_for_offset_range(coin_id: int, offset: int, limit: int, cursor=None):
    sql = "SELECT my_date FROM `timeseries` WHERE coin_id = %s LIMIT %s OFFSET %s"
    cursor.execute(sql, [coin_id, limit, offset])
    return [x['my_date'] for x in list(cursor.fetchall())]


@connection_wrap
def get_all_coins_raw(cursor=None):
    sql = "SELECT * FROM coin"
    cursor.execute(sql)
    return cursor.fetchall()


@connection_wrap
def get_urls(name, cursor=None):
    name = pymysql.escape_string(name)
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return None
    sql = (
        "SELECT `website_url`, `whitepaper_url` "
        "FROM coin "
        "WHERE `coin_id` = %d"
        % (coin_id)
    )
    cursor.execute(sql)
    ret = cursor.fetchone()

    return ret


@connection_wrap
def get_coin_id_symbols(cursor=None):
    sql = "SELECT `coin_id`, `ticker_symbol` FROM coin "
    cursor.execute(sql)
    ret = cursor.fetchall()

    return {x['ticker_symbol']: x['coin_id'] for x in ret}
