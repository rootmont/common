from db.connect import connection_wrap, get_latest_date, timing, parse_frequency
from db.coin import get_coin_id_for_symbol, get_coin_id_for_name, get_name_for_coin_id
from db import risk
import pymysql
import pandas as pd
import datetime
import numbers
import logging


logger = logging.getLogger('db')
risk_data = None
last_risk_update = None
default_days_back = 90
twelve_hours = 60 * 60 * 12


@connection_wrap
def get_latest_timeseries_raw(coin_id: int, cursor=None):
    sql = "SELECT max(my_date) as max_date FROM timeseries WHERE coin_id = %s"
    cursor.execute(sql, coin_id)
    ret = cursor.fetchone()
    if ret is None:
        return None
    sql = (
            "SELECT * FROM timeseries WHERE coin_id = %s AND my_date = %s"
    )
    cursor.execute(sql, [coin_id, ret["max_date"]])
    return cursor.fetchone()


@connection_wrap
def get_all_supplies(symbols, cursor=None):
    if type(symbols) == str: symbols = [symbols]
    escaped = [pymysql.escape_string(x) for x in symbols]
    condition = ' OR '.join(["ticker_symbol = '%s'" % x for x in escaped])
    
    
    sql = (
            "SELECT supply, my_date, c.token_name "
            "FROM timeseries t "
            "INNER JOIN coin c "
            "ON t.coin_id = c.coin_id "
            "WHERE %s "
            % (condition)
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    
    df = pd.DataFrame(ret, columns=["supply", "my_date", "token_name"])
    return pd.pivot_table(df, index='my_date', columns='token_name', values='supply', fill_value=0)


@connection_wrap
def get_all_prices_for_date(datestring, cursor=None):
    
    sql = (
            "SELECT price_close, ticker_symbol "
            "FROM timeseries "
            "INNER JOIN coin "
            "ON coin.coin_id = timeseries.coin_id "
            "WHERE my_date = '%s' "
            % (datestring)
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    if ret is None: ret = {}
    df = pd.Series(data=[x["price_close"] for x in ret], index=[x["ticker_symbol"] for x in ret])
    return df


@connection_wrap
def get_all_mcaps_for_date(datestring, cursor=None):

    sql = (
            "SELECT marketcap, ticker_symbol "
            "FROM timeseries "
            "INNER JOIN coin "
            "ON coin.coin_id = timeseries.coin_id "
            "WHERE my_date = '%s' "
            % (datestring)
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    if ret is None: ret = {}
    df = pd.Series(data=[x["marketcap"] for x in ret], index=[x["ticker_symbol"] for x in ret])
    return df


@connection_wrap
def insert_price(coin_id, date_string, price_open, price_low, price_high, price_close, trading_volume, cursor=None):
    name = get_name_for_coin_id(coin_id)
    columns = ','.join([
        'coin_id',
        'token_name',
        'my_date',
        'price_open',
        'price_low',
        'price_high',
        'price_close',
        'trading_volume'
    ])
    values = (coin_id, name, date_string, price_open, price_low, price_high, price_close, trading_volume)
    sql = (
            "INSERT INTO `timeseries` (%s) "
            "VALUES %s "
            % (columns, str(values))
    )
    sql += (
        "ON DUPLICATE KEY UPDATE "
        "%s = %s, %s = %s, %s = %s, %s = %s, %s = %s, %s = '%s' "
    ) % ('price_open', price_open,
         'price_low', price_low,
         'price_high', price_high,
         'price_close', price_close,
         'trading_volume', trading_volume,
         'token_name', name)

    cursor.execute(sql)


@connection_wrap
def insert_price_2(coin_id, date_string, price_open, price_low, price_high, price_close, trading_volume, cursor=None):
    name = get_name_for_coin_id(coin_id)
    sett = ','.join([
        '`token_name` = "%s"' % name,
        '`price_open` = %f' % float(price_open),
        '`price_low` = %f' % float(price_low),
        '`price_high` = %f' % float(price_high),
        '`price_close` = %f' % float(price_close),
        '`trading_volume` = %f' % float(trading_volume)
    ])
    sett2 = ','.join([
        '`coin_id` = %d' % coin_id,
        '`my_date` = DATE("%s")' % date_string,
        sett
    ])
    sql = (
            'INSERT INTO `timeseries` SET %s '
            'ON DUPLICATE KEY UPDATE %s '
    ) % (sett2, sett)

    cursor.execute(sql)


@connection_wrap
def get_latest_prices(cursor=None):
    
    sql = (
            "SELECT max(my_date) as max_date, token_name "
            "FROM timeseries "
            "WHERE price_close is not NULL "
            "GROUP BY token_name "
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    
    df = pd.Series(data=[x['max_date'] for x in ret], index=[x["token_name"] for x in ret])
    return df


@connection_wrap
def get_latest_ntxs(cursor=None):
    
    sql = (
            "SELECT max(my_date) as max_date, token_name "
            "FROM timeseries "
            "WHERE daily_transactions is not NULL "
            "GROUP BY token_name "
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    
    df = pd.Series(data=[x['max_date'] for x in ret], index=[x["token_name"] for x in ret])
    return df


@connection_wrap
def get_earliest_ntxs(cursor=None):
    
    sql = (
            "SELECT min(my_date) as min_date, token_name "
            "FROM timeseries "
            "WHERE daily_transactions is not NULL "
            "GROUP BY token_name "
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    
    df = pd.Series(data=[x['min_date'] for x in ret], index=[x["token_name"] for x in ret])
    return df


@connection_wrap
def get_latest_supplies(cursor=None):
    
    sql = (
            "SELECT max(my_date) as max_date, ticker_symbol "
            "FROM timeseries "
            "INNER JOIN coin "
            "ON coin.coin_id = timeseries.coin_id "
            "WHERE supply > 0 "
            "GROUP BY ticker_symbol "
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    
    df = pd.Series(data=[x['max_date'] for x in ret], index=[x["ticker_symbol"] for x in ret])
    return df


@connection_wrap
def get_risk_inputs(end_date=datetime.date.today(), days_back=default_days_back, age_cluster=None, mcap_cluster=None, industry_cluster=None, cursor=None):
    df = get_all_risk_inputs()
    if industry_cluster is not None:
        df = df[df['industry_cluster'] == industry_cluster]
    if age_cluster is not None:
        df = df[df['age_cluster'] == age_cluster]
    if mcap_cluster is not None:
        df = df[df['marketcap_cluster'] == mcap_cluster]
    start_date = end_date - datetime.timedelta(days=days_back)
    # end_date not included because today will generally have incomplete data
    df = df[(df['my_date'] >= start_date) & (df['my_date'] < end_date)]
    return df


@connection_wrap
def get_all_risk_inputs(cursor=None):
    global risk_data
    global last_risk_update
    # caching with nyquist frequency (sampling_rate > 2 * target_frequency)
    if risk_data is not None and (datetime.datetime.now() - last_risk_update).seconds < twelve_hours:
        return risk_data

    sql = (
        "SELECT c.token_name, t.my_date, t.price_close, t.marketcap, c.marketcap_cluster, c.age_cluster, cc.category as industry_cluster "
        "FROM timeseries t "
        "INNER JOIN coin c "
        "ON c.coin_id = t.coin_id "
        "INNER JOIN coin_category cc "
        "ON cc.coin_id = t.coin_id "
        "ORDER BY `my_date` ASC"
    )
    
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    
    ret = pd.DataFrame(ret, columns=["price_close", "marketcap", "my_date", "token_name", "marketcap_cluster", "age_cluster", "industry_cluster"])

    # update cache
    risk_data = ret
    last_risk_update = datetime.datetime.now()

    return ret


@connection_wrap
def insert_ntx(name, datestring, ntxs, cursor=None):
    if type(datestring) != datetime.date:
        datestring = datetime.datetime.strptime(datestring, '%Y/%m/%d').date()
#        datestring = datestring.strftime('%Y/%m/%d')
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return None
    args = (int(ntxs), name, coin_id, datestring, int(ntxs), name)
    sql = (
        "INSERT INTO `timeseries` SET "
        "`daily_transactions` = %s, "
        "`token_name` = %s, "
        "`coin_id` = %s, `my_date` = %s "
        "ON DUPLICATE KEY UPDATE "
        "`daily_transactions` = %s, "
        "`token_name` = %s "
    )
    # logger.info('%s: %s' % (sql, args))
    cursor.execute(sql, args)


@connection_wrap
def get_ntxs(end_date=datetime.date.today(), days_back=1, cursor=None):
    start_date = end_date - datetime.timedelta(days=days_back)
    sql = (
            "SELECT daily_transactions, my_date, c.token_name "
            "FROM timeseries t "
            "INNER JOIN coin c "
            "ON c.coin_id = t.coin_id "
            "WHERE my_date >=  DATE('{}') "
            "AND my_date <= DATE('()') "
    ).format(start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d'))
    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.DataFrame(ret, columns=["daily_transactions", "my_date", "token_name"])
    return pd.pivot_table(df, index='my_date', columns='token_name', values='daily_transactions', fill_value=0)


@connection_wrap
def get_all_ntxs(symbols, cursor=None):
    if type(symbols) == str:
        symbols = [symbols]
    escaped = [pymysql.escape_string(x) for x in symbols]
    condition = ' OR '.join(["ticker_symbol = '%s'" % x for x in escaped])

    sql = (
            "SELECT daily_transactions, my_date, c.token_name "
            "FROM timeseries t "
            "INNER JOIN coin c "
            "ON c.coin_id = t.coin_id "
            "WHERE %s "
            % (condition)
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    
    df = pd.DataFrame(ret, columns=["daily_transactions", "my_date", "token_name"])
    return pd.pivot_table(df, index='my_date', columns='token_name', values='daily_transactions', fill_value=0)


@connection_wrap
def get_all_ntxs_name(name, cursor=None):
    escaped = pymysql.escape_string(name)
    condition = "token_name = '{}'".format(escaped)

    sql = (
            "SELECT daily_transactions, my_date "
            "FROM timeseries t "
            "INNER JOIN coin c "
            "ON c.coin_id = t.coin_id "
            "WHERE %s "
            % (condition)
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.DataFrame(ret, columns=["daily_transactions", "my_date", "token_name"])
    df.index = df['my_date']
    df.drop(columns=['my_date'], inplace=True)
    return df


@timing
@connection_wrap
def get_missing_ntxs(cursor=None):
    
    sql = (
        "SELECT `my_date`, `token_name`, `daily_transactions`, `price_close` "
        "FROM timeseries "
        "WHERE daily_transactions is NULL "
        "AND price_close > 0 order by `my_date` desc LIMIT 1000"
    )
    logger.debug(sql)
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    
    df = pd.DataFrame(ret, columns=['my_date','token_name','daily_transactions', 'price_close'])
    return df


def parse_filter(filter):
    clauses = []
    where = ''
    try:
        for col in filter:
            if 'age' in col:
                clauses.append('(%s)' % (' OR '.join([' age_cluster = "%s"' % pymysql.escape_string(x) for x in col['age']])))
            if 'marketcap' in col:
                clauses.append('(%s)' % (' OR '.join([' marketcap_cluster = "%s"' % pymysql.escape_string(x) for x in col['marketcap']])))
            if 'industry' in col:
                clauses.append('(%s)' % (' OR '.join([' category = "%s"' % pymysql.escape_string(x) for x in col['industry']])))
            if 'token_name' in col:
                clauses.append('(%s)' % (' OR '.join([' timeseries.token_name = "%s"' % pymysql.escape_string(x) for x in col['token_name']])))
            if 'dates' in col:
                min_date = pymysql.escape_string(col['dates'][0].strftime('%Y/%m/%d'))
                max_date = pymysql.escape_string(col['dates'][1].strftime('%Y/%m/%d'))
                clauses.append('timeseries.my_date >= DATE("%s") AND timeseries.my_date <= DATE("%s")' % (min_date, max_date))

            # col = {'metric', 'min', 'max', 'days back', 'percentile'}
            if 'metric' in col:
                name = col['metric']
                if 'days back' in col:
                    name += '_%s' % col['days back']
                if 'percentile' in col and col['percentile'] == True:
                    name += '_rank_global'
                elif col['metric'] in risk.risk_columns:
                    name += '_global'
                if 'marketcap' in name: name = 'timeseries.' + name
                name = pymysql.escape_string(name)
                if 'min' in col and isinstance(col['min'], numbers.Number):
                    clauses.append('%s >= %f' % (name, col['min']))
                if 'max' in col and isinstance(col['max'], numbers.Number):
                    clauses.append('%s <= %f' % (name, col['max']))
        where = ' AND '.join(clauses)
        if filter is None:
            logger.error('Interpreting filter {} as an empty filter'.format(filter))
            where = ' TRUE '
    except Exception:
        logger.error('Bad filter: %s ' % filter)
    return where


@connection_wrap
def filter_coins_date(date, filter, cursor=None):
    where = parse_filter(filter)
    datestring = date.strftime('%Y/%m/%d')
    
    sql = (
        "SELECT timeseries.token_name "
        "FROM timeseries "
        "INNER JOIN risk "
        "ON risk.coin_id = timeseries.coin_id "
        "AND risk.my_date = timeseries.my_date "
        "INNER JOIN coin "
        "ON coin.coin_id = timeseries.coin_id "
        "INNER JOIN coin_category "
        "ON coin_category.coin_id = coin.coin_id "
        "WHERE timeseries.my_date = DATE('%s') "
        "AND %s "
        % (datestring, where)
    )
    cursor.execute(sql)
    ret = list(cursor.fetchall())
    
    return [x['token_name'] for x in ret if x is not None]


@connection_wrap
def filter_coins(filter, frequency, cursor=None):
    where = parse_filter(filter)
    freq = parse_frequency(frequency)
    sql = (
            "SELECT timeseries.my_date, timeseries.token_name "
            "FROM timeseries "
            "INNER JOIN risk "
            "ON risk.coin_id = timeseries.coin_id "
            "AND risk.my_date = timeseries.my_date "
            "INNER JOIN coin "
            "ON coin.coin_id = timeseries.coin_id "
            "INNER JOIN coin_category "
            "ON coin_category.coin_id = coin.coin_id "
            "INNER JOIN ranks "
            "ON ranks.coin_id = timeseries.coin_id "
            "AND ranks.my_date = timeseries.my_date "
            "INNER JOIN price_daily "
            "ON price_daily.coin_id = timeseries.coin_id "
            "AND price_daily.my_date = timeseries.my_date "
            "WHERE price_daily.price_close > 0 "
            "AND %s "
            "AND %s "
            % (where, freq)
    )
    logger.debug(sql)
    cursor.execute(sql)
    ret = list(cursor.fetchall())

    df = pd.DataFrame(ret, columns=['my_date', 'token_name'])
    return df


@connection_wrap
def insert(item, cursor=None):
    name, cols = item
    coin_id = get_coin_id_for_name(name)
    if coin_id is None: return None
    datestring = cols['date'].strftime('%Y/%m/%d')
    del(cols['date'])
    set = ','.join(['%s = %s' % (k,round(v,14)) for k,v in cols.items()])
    sql = (
        "UPDATE timeseries "
        "SET %s "
        "WHERE coin_id = %d "
        "AND `my_date` = DATE ('%s') "
        % (set, coin_id, datestring)
    )
    
    logger.debug('Attempting %s' % sql)
    cursor.execute(sql)
    logger.debug('Successful %s' % sql)


@connection_wrap
def get_columns_for_coin_date(columns, name, date, cursor=None):
    coin_id = get_coin_id_for_name(name)
    if coin_id is None: return None
    select = ','.join(columns)
    datestring = date.strftime('%Y/%m/%d')
    sql = (
        "SELECT %s "
        "FROM timeseries "
        "WHERE my_date = DATE('%s') "
        "AND coin_id = %d "
        % (select, datestring, coin_id)
    )
    
    cursor.execute(sql)
    ret = cursor.fetchone()
    
    return ret


@connection_wrap
def get_overall_ranks_for_date(date=None, cursor=None):
    if date is None:
        date = get_latest_date(table='timeseries',metric='usage_90_rank_global')
    datestring = date.strftime('%Y/%m/%d')
    sql = (
        "SELECT marketcap_90_rank_global as price_rank, "
        "usage_90_rank_global as usage_rank, "
        "token_name "
        "FROM timeseries "
        "WHERE my_date = %s "
        "AND token_name IS NOT NULL "
    )
    cursor.execute(sql, (datestring))
    ret = list(cursor.fetchall())
    df = pd.DataFrame(data=ret, columns=['price_rank','usage_rank'], index=[x['token_name'] for x in ret])
    return df



@connection_wrap
def get_recent_transactions(end_date=datetime.date.today(), days_back=default_days_back, age_cluster=None, mcap_cluster=None, industry_cluster=None, cursor=None):
    logger.debug('Getting recent transactions for {}, starting {} days back'.format(end_date, days_back))
    condition = ""
    if age_cluster is not None:
        age_cluster = pymysql.escape_string(age_cluster)
        condition += "AND c.age_cluster = '%s' " % age_cluster
    if mcap_cluster is not None:
        mcap_cluster = pymysql.escape_string(mcap_cluster)
        condition += "AND c.marketcap_cluster = '%s' " % mcap_cluster
    if industry_cluster is not None:
        industry_cluster = pymysql.escape_string(industry_cluster)
        condition += "AND cc.category = '%s' " % industry_cluster

    start_datestring = (end_date - datetime.timedelta(days_back)).strftime("%Y/%m/%d")
    end_datestring = end_date.strftime("%Y/%m/%d")
    # upper bound is strictly less than because we may have incomplete data for today
    sql = (
            "SELECT `token_name`, `my_date`, `price_close`, `daily_transactions`, `trading_volume`, `marketcap` "
            "FROM timeseries t "
            "INNER JOIN coin_category cc "
            "ON cc.coin_id = t.coin_id "
            "WHERE `my_date` >= '%s' "
            "AND `my_date` < '%s' "
            "%s"
            "ORDER BY `my_date` ASC"
            % (start_datestring, end_datestring, condition)
    )
    
    print(sql)

    cursor.execute(sql)
    ret = [x for x in cursor.fetchall() if x['token_name'] is not None]
    
    df = pd.DataFrame(ret, columns=["price_close", "daily_transactions", "my_date", "token_name", "trading_volume", "marketcap"])
    df.index = df["my_date"]

    return df