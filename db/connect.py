import pymysql
from common.config import format_logger
from functools import wraps
from hashlib import sha3_256 as sha3
import logging
import time
from typing import Union

from env import DBHOST, DBUSER, DBPASS, DBDATABASE, DBPORT

logger = logging.getLogger('db')
format_logger(logger)

connection_details = {
    "host": DBHOST,
    "user": DBUSER,
    "password": DBPASS,
    "db": DBDATABASE,
    "port": int(DBPORT),
    "charset": "utf8mb4",
    "cursorclass": pymysql.cursors.DictCursor,
}
connection = None


def retry(func):
    def call(*args, **kwargs):
        for i in range(10):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                logger.warning('Could not execute {} on {} try'.format(func, i))
        return func(*args, **kwargs)
    return call


def log_sensitive(var_name, sensitive, fxn=logger.info):
    dig = sha3(str(sensitive).encode()).hexdigest()
    msg = 'SHA3(%s) = %s' % (var_name, dig)
    fxn(msg)


@retry
def get_new_connection():
    try:
        return pymysql.connect(**connection_details)
    except pymysql.err.OperationalError as e:
        logger.fatal(e)
        log_sensitive('DBHOST', DBHOST, logger.fatal)
        log_sensitive('DBUSER', DBUSER, logger.fatal)
        log_sensitive('DBPASS', DBPASS, logger.fatal)
        log_sensitive('DBDATABASE', DBDATABASE, logger.fatal)
        log_sensitive('DBPORT', DBPORT, logger.fatal)
        raise e


def connection_wrap(func):
    def call(*args, **kwargs):
        result = None
        cursor = None
        connection = get_new_connection()
        try:
            cursor = connection.cursor()
            result = func(*args, **kwargs, cursor=cursor)
            connection.commit()
        except Exception as e:
            logger.error(e)
        finally:
            if cursor is not None:
                cursor.close()
            # connection.close()
        return result
    return call


@connection_wrap
def get_latest_date(metric: str, table: str, coin_id: Union[None, int]=None, cursor=None):
    sql = (
        'SELECT max(my_date) as max_date ' 
        'FROM %s ' 
        'WHERE %s IS NOT NULL '
    ) % (table, metric)
    if coin_id is not None:
        sql += ' AND coin_id = %d ' % coin_id
    logger.debug(sql)
    cursor.execute(sql)
    ret = cursor.fetchone()
    return ret.get('max_date')


def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()
        logger.info('func:%r took: %2.4f sec' % (f.__name__, te-ts))
        return result
    return wrap


@connection_wrap
def get_columns(table, cursor=None):
    sql = 'desc ' + table
    cursor.execute(sql)
    ret = cursor.fetchall()
    cols = [x['Field'] for x in ret]
    return cols


def parse_frequency(frequency):
    if frequency == 'days':
        return ' TRUE'
    if frequency == 'weeks':
        return ' DAYOFWEEK(timeseries.my_date) = 1 '
    if frequency == 'months':
        return ' DAY(timeseries.my_date) = 1 '
    if frequency == 'years':
        return " DAY(timeseries.my_date) = 1 AND MONTH(timeseries.my_date) = 1 "
    logger.error('Received invalid frequency: %s. Unable to parse.' % frequency)
