import logging
from typing import Dict

from db.coin import get_coin_id_for_name
from db.connect import connection_wrap, timing
import pymysql

logger = logging.getLogger('daily stats')

def _insert(coin_stat, cursor=None):
    column_names = []
    values = []
    for k in coin_stat:
        column_names.append(k)
        values.append(coin_stat[k])
    sql = "INSERT INTO daily_stats (" + ", ".join(column_names) + \
          ") VALUES (" + ", ".join(["%s" for _ in range(0, len(values))]) + ")"
    cursor.execute(sql, values)


@timing
def insert_update_all(coin_stats: Dict):
    for coin_id_key in coin_stats:
        _insert_update(coin_stats[coin_id_key])
    for coin_stat in get_all():
        _update_null(coin_stat)


@connection_wrap
def get_all(cursor=None):
    sql = "SELECT * FROM daily_stats"
    cursor.execute(sql)
    return cursor.fetchall()


@connection_wrap
def _update_null(coin_stat, cursor=None):
    cols = []
    sql = "UPDATE daily_stats SET "
    for k in coin_stat:
        if coin_stat[k] is None:
            cols.append(k + " = 0.0")
    sql += ", ".join(cols) + " WHERE token_name = %s"
    if len(cols) > 0:
        logger.debug(sql)
        cursor.execute(sql, coin_stat["token_name"])


@connection_wrap
def _get_row(name, cursor=None):
    coin_id = get_coin_id_for_name(name)
    if coin_id is None:
        return None
    sql = "SELECT * FROM daily_stats WHERE coin_id = {} LIMIT 1".format(int(coin_id))
    logger.debug(sql)
    cursor.execute(sql)
    ret = cursor.fetchone()
    return ret


@connection_wrap
def _get_catalog(age_cluster=None, mcap_cluster=None, industry_cluster=None, cursor=None):
    where = ''
    if age_cluster is not None:
        where = 'WHERE age_cluster = "%s"' % pymysql.escape_string(age_cluster)
    elif mcap_cluster is not None:
        where = 'WHERE marketcap_cluster = "%s"' % pymysql.escape_string(mcap_cluster)
    elif industry_cluster is not None:
        where = 'WHERE industry_cluster = "%s"' % pymysql.escape_string(industry_cluster)

    sql = (
        'SELECT token_name as name, age_cluster, industry_cluster, marketcap_cluster, risk_rank as "price percentile", social_rank as "social percentile", '
        'usage_rank as "usage percentile", dev_rank as "development percentile", '
        'root_rank as "overall percentile", marketcap, ticker_symbol as symbol, price_close as price, '
        'price_24h_pct as 24h FROM daily_stats '
        '%s'
    ) % where
    logger.debug(sql)
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


@connection_wrap
def _insert_update(coin_stat, cursor=None):
    sql = "INSERT INTO daily_stats SET "
    missing_cols = []
    sets = []
    for k in coin_stat:
        # if k != 'price_close' and k != 'coin_id': continue
        if coin_stat[k] is None:
            missing_cols.append(k)
        elif type(coin_stat[k]) == str:
            sets.append('`{}` = "{}"'.format(k, coin_stat[k]))
        else:
            sets.append('`{}` = {}'.format(k, coin_stat[k]))
    sql += ", ".join(sets)
    sql += " ON DUPLICATE KEY UPDATE " + ", ".join(sets)

    logger.debug(sql)
    # this is apparently necessary (no idea why), otherwise the insert/update won't work :/
    cursor.execute('select `token_name`, `price_close` from daily_stats where `token_name` = "{}"'.format(coin_stat['token_name']))
    # a = cursor.fetchall()
    # logger.debug(a)
    num_rows = cursor.execute(sql)
    # if num_rows == 0:
    #     logger.warning('Update failed for {} in daily_stats table!'.format(coin_stat['token_name']))
    # logger.debug('Rows Updated: {}'.format(num_rows))
    if len(missing_cols) > 0:
        logger.warning("Missing columns for {} in daily_update: [ {} ]"
                       .format(coin_stat["token_name"], ", ".join(missing_cols)))


@connection_wrap
def get_metcalfe_for_name(token_name, cursor=None):
    coin_id = get_coin_id_for_name(token_name)
    if coin_id is None: return None
    sql = (
            "SELECT metcalfe_exponent_10000, metcalfe_proportion_10000 "
            "FROM daily_stats "
            "WHERE coin_id = %s "
    )

    cursor.execute(sql, (coin_id))
    ret = cursor.fetchone()

    return ret


@connection_wrap
def get_coin_vectors(cursor=None):

    sql = (
        "SELECT token_name as name, risk_rank as risk, industry_cluster as industry, marketcap_cluster as marketcap, "
        "age_cluster as age, dev_rank as dev, social_rank as social, usage_rank as 'usage', "
        "marketcap_90_rank_global as price, "
        "up_down_90_rank_global as updown "
        "FROM daily_stats "
    )
    cursor.execute(sql)
    ret = cursor.fetchall()
    return ret


